import sys
from cx_Freeze import setup, Executable


build_exe_options = {"packages": ["lxml"],
                     'build_exe': 'SBArchiver'}


##base = None
##if sys.platform == "win32":
##    base = "Win32GUI"

setup(  name = "SB to json Archiver",
        version = "0.1",
        description = "Archiver",
        options = {"build_exe": build_exe_options},
        executables = [Executable("archiver3.py", targetName="Archiver.exe")])