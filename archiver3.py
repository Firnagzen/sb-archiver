from time import sleep
import XenforoToBBCode as XF
import os, re, sys, json, requests, multiprocessing

baseURL = "http://forums.spacebattles.com/threads/"

title_re = re.compile('[\W_]+')

XFparser = XF.XenforoToBBCode()

def get_page(URL):
    print("Getting " + URL)
    while True:
        try:
            x = requests.get(URL)
        except requests.exceptions.RequestException:
            print("Encountered a requests error! Sleeping for 30s...")
            sleep(30)
        else:
            status, h = XFparser.inspect_page(x.content)
            if status:
                last_page = h[0]
                parsed_page = h[1]
                break
            else:
                if "The requested thread could not be found." in h:
                    print("Invalid thread! Check your URL...")
                    break
                elif "error has occurred" in h:
                    print("An 'unexpected' error has occured. "
                          "Sleeping for 30s...")
                    sleep(30)
    return parsed_page, last_page

def handle_page(parsed_page, usernames, first=False):
    handled = XFparser.break_page(parsed_page, usernames)
    try:
        title = title_re.sub('', handled[0]['ThreadName'])
    except IndexError:
        return ""

    if first:
        with open(title + ".txt", "w") as f:
            f.write('[\n')

    with open(title + ".txt", "a") as f:
        for i in handled:
            f.write(json.dumps(i, indent=4, separators=(',', ': ')) + "\n,\n")

    return title

def arg_iter(less_URL, last_page, usernames):
    for i in range(1, int(last_page) + 1):
        yield less_URL + '/page-' + str(i), usernames

def worker_tar(arg):
    URL, usernames = arg
    parsed_page, _ = get_page(URL)
    handle_page(parsed_page, usernames)

def get_pastelist():
    pb = requests.get("http://pastebin.com/raw.php?i=WHATEVER")
    return pb.content.decode().splitlines()

                
if __name__ == "__main__":
    multiprocessing.freeze_support()
    
    def get_thread(thread_no, usernames):
        less_URL = baseURL + thread_no
        parsed_page, last_page = get_page(less_URL)
        title = handle_page(parsed_page, usernames, first=True)

        lpool = multiprocessing.Pool(processes = 5)
        lpool.map(worker_tar, arg_iter(less_URL, last_page, usernames))

        # for i in range(1, int(last_page) + 1):
        #     parsed_page, _ = get_page(less_URL + '/page-' + str(i))
        #     handle_page(parsed_page, usernames)

        with open(title + ".txt", 'rb+') as f:
            f.seek(-3, 2)
            f.truncate()

        with open(title + ".txt", "a") as f:
            f.write(']')

    thread_nos = input('Please enter the thread number(s), separated by commas'
                       ' without spacing: ')
    usernames = input('Please enter a comma separated list of usernames to'
                      ' scrape. Do not space the commas: ')
    usernames = set(usernames.split(','))

    for i in thread_nos.split(','):
        get_thread(i, usernames)
