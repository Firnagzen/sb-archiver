import re, requests
from io import BytesIO
from html import entities
import lxml.etree as etree
from time import strptime, sleep, mktime

class XenforoToBBCode(object):
    """
    Parses xHTML from Xenforo to BBCode, returning a dictionary of properties
    from the posts
    """

    post_xpath = etree.XPath(".//li[@data-author]")

    end_xpath = etree.XPath(".//div[@class='PageNav']")

    magical_parser = etree.XMLParser(encoding='utf-8', recover=True)

    post_number_xpath = etree.XPath(".//a[@class='item muted postNumber"
                                    " hashPermalink OverlayTrigger']/text()")

    post_content_xpath = etree.XPath(".//blockquote[@class = "
                                     "'messageText ugc baseHtml']")

    message_meta_xpath = etree.XPath(".//div[@class='messageMeta']")

    titlebar_xpath = etree.XPath(".//div[@class='titleBar']")

    likes_xpath = etree.XPath(".//div[@class='likesSummary "
                              "secondaryContent']/span/a")

    like_page_xpath = etree.XPath(".//h3[@class='username']")

    error_xpath = etree.XPath(".//div[@class='errorOverlay']/div/label")

    end_xpath = etree.XPath(".//div[@class='PageNav']")

    blockquote_xpath = etree.XPath(".//blockquote")

    cleanup_re = ('\[(color|font|size|URL|media|quote)[^=]*=[\"\']*'
                  '([^>\"\']*)[\"\']*\]')

    clea_xpath = etree.XPath(".//span[contains(@style, 'size') or "
                                     "contains(@style, 'color') or "
                                     "contains(@style, 'font') or "
                                     "contains(@style, 'text-decoration')]|"
                             ".//div[contains(@style, 'left') or"
                                    "contains(@style, 'right') or"
                                    "contains(@style, 'center') or"
                                    "@class='bbCodeBlock bbCodeQuote' or"
                                    "@class='bbCodeBlock bbCodeCode']|"
                             ".//a[@class='externalLink' or"
                                  "@class='internalLink']|"
                             ".//img|"
                             ".//iframe")

    text_deco_re = re.compile("(color|size|font)(?:-family)?: (.*)")

    text_align_re = re.compile("(?:text-align|padding-left): (\w*)")

    youtube_re = re.compile("(?:youtu\.be\/|youtube\.com\/(?:watch\?(?:.*&)?v=|"
                            "(?:embed|v)\/))([^\?&\"'>]+)")

    posterID_re = re.compile("members/[^\.]*\.([\d]*)/")

    postID_re = re.compile("posts/([0-9]*)/permalink")

    threadID_re = re.compile("threads/[^\.]*\.([\d]*)/")

    def __init__(self):
        pass
        
    @classmethod
    def clean_text(cls, raw):
        """
        Takes a li and iterates through it, converting HTML tags to
        BBCode. Unrecognized nodes are not touched. Returns post contents.
        """
        try:
            cls.dispatch
        except AttributeError:
            cls.dispatch = {
                            "span" : cls.span_deco,
                            "text-decoration: underline" : cls.span_underline,
                            "text-decoration: "
                            "line-through" :cls.span_strikethrough,
                            "div" : cls.div,
                            "a" : cls.a,
                            "iframe" : cls.iframe,
                            "img" : cls.img,
                            "bbCodeBlock bbCodeQuote" : cls.quote,
                            "bbCodeBlock bbCodeCode" : cls.code,
                            "xx-small" : "1", "x-small" : "2", "small" : "3", 
                            "medium" : "4", "large" : "5", "x-large" : "6", 
                            "xx-large" : "7"
                            }

        proc = cls.post_content_xpath(raw)[0]

        for elem in cls.clea_xpath(proc):
            try:
                cls.dispatch[elem.tag](elem)
            except KeyError:
                print(cls._stringify(elem) + elem.tag + 
                      " not accounted for in the dispatch.")

        etree.strip_elements(proc, 'br', with_tail = False)
        etree.strip_elements(proc, 'script', with_tail = False)
        etree.strip_tags(proc, 'remove')

        stringy =  cls._stringify(proc)[90:]


        return stringy.strip()
        
    @classmethod
    def _stringify(cls, item):
        try:
            cls.stringified = etree.tostring(item, encoding='utf-8').decode()
        except TypeError:
            cls.stringified = item.decode()
        cls.stringified = re.sub("<([^>]*)>", "[\\1]", cls.stringified)
        cls.stringified = re.sub("\[(/?q)\]", "[\\1uote]", cls.stringified)
        cls.stringified =re.sub("\n{5}|\[/blockquote\]\n*","\n",cls.stringified)
        return re.sub(cls.cleanup_re, "[\\1='\\2']", cls.stringified)

    @classmethod
    def iframe(cls, elem):
        cls.yturl = cls.youtube_re.search(elem.attrib['src']).group(1)
        elem.attrib.clear()
        elem.tag = "media"
        elem.attrib["tmp"] = "youtube"
        elem.text = cls.yturl

    @classmethod
    def img(cls, elem):
        if elem.attrib['src'].startswith('styles'):
            try:
                elem.tail = elem.attrib['alt'] + elem.tail
            except TypeError:
                elem.tail = elem.attrib['alt']
            elem.tag = 'remove'
        else:
            elem.text = elem.attrib['src']
            elem.attrib.clear()

    @classmethod
    def span_deco(cls, elem):
        cls.style = elem.attrib["style"]
        try:
            cls.dispatch[cls.style](elem)
        except KeyError:
            cls.deco_type = cls.text_deco_re.findall(cls.style)
            try:
                elem.tag = cls.deco_type[0][0]
            except IndexError:
                return
            try:
                elem.attrib["style"] = cls.dispatch[cls.deco_type[0][1]]
            except KeyError:
                elem.attrib["style"] = cls.deco_type[0][1]
        
    @classmethod
    def span_underline(cls, elem):
        elem.tag = "u"
        elem.attrib.pop("style")
        
    @classmethod
    def span_strikethrough(cls, elem):
        elem.tag = "s"
        elem.attrib.pop("style")
        
    @classmethod
    def div(cls, elem):
        try:
            align = cls.text_align_re.search(elem.attrib['style'])
        except KeyError:
            # This is a quote or code block
            try:
                cls.dispatch[elem.attrib['class']](elem)
            except KeyError:
                print(cls._stringify(elem) + elem.tag + 
                      " not accounted for in the dispatch.")
        else:
            # Not a quote block
            if align and align.group(1) == '30px':
                elem.attrib.pop('style')
                elem.tag = 'indent'
            elif align:
                elem.attrib.pop('style')
                elem.tag = align.group(1)
            else:
                elem.tag = "remove"
                try:
                    elem.tail = "\n" + elem.tail
                except TypeError:
                    elem.tail = "\n"

    @classmethod
    def quote(cls, elem):
        quoted_content = cls.blockquote_xpath(elem)[0]

        try:
            quoter = elem.attrib['data-author']
        except KeyError:
            quoter = None
        quot_tail = elem.tail

        elem.clear()
        elem.tag = 'remove'

        quoted_content.tag = 'quote'
        quoted_content.tail = quot_tail
        if quoter:
            quoted_content.attrib['tmp'] = quoter
        elem.addprevious(quoted_content)

    @classmethod
    def code(cls, elem):
        code_content = elem[1]
        code_tail = elem.tail

        elem.clear()
        elem.tag = 'remove'

        code_content.tag = 'code'
        code_content.tail = code_tail

        elem.addprevious(code_content)


    @classmethod
    def a(cls, elem):
        elem.tag = "URL"
        for i in elem.attrib:
            if i != "href":
                elem.attrib.pop(i)

    @classmethod
    def page_info(cls, page):
        tb = cls.titlebar_xpath(page)
        return {
        "ThreadName" : tb[0][0].text,
        "ThreadID" : cls.threadID_re.search(tb[0][1][2].attrib['href']).group(1)
        # "OP" : tb[0][1][1].text,
        # "OPID" : cls.posterID_re.search(tb[0][1][1].attrib['href']).group(1),
        }

    @classmethod
    def post_info(cls, post, usernames):
        meta = cls.message_meta_xpath(post)[0]
        usin = meta[0][0]
        poin = meta[1]
        
        likeslist = cls.likes_xpath(post)

        if usin[0].text not in usernames:
            raise ValueError

        info = {
            'Username': usin[0].text,
            'UserID' : cls.posterID_re.search(usin[0].attrib['href']).group(1),
            'PostID':cls.postID_re.search(poin[0].attrib['data-href']).group(1),
            'Likes': []
                }
        try:
            info['PostDate'] = usin[1][0].attrib['data-time']
        except KeyError:
            # strptime("May 29, 2013 at 8:10 PM", "%b %d, %Y at %I:%M %p")
            strdate = usin[1][0].attrib['title']
            info['PostDate'] =mktime(strptime(strdate, "%b %d, %Y at %I:%M %p"))

        for i in reversed(likeslist):
            try:
                likedict = {
                    'Username' : i.text,
                    'UserID' : cls.posterID_re.search(i.attrib['href']).group(1)
                            }
            except AttributeError:
                info['Likes'] = cls.likes_info(i.attrib['href'])
                break
            info['Likes'].append(likedict)

        return info

    @classmethod
    def likes_info(cls, likeURL):
        likes = []

        print("Getting likes for " + likeURL)

        while True:
            try:
                x = requests.get("http://forums.spacebattles.com/" + likeURL)
            except requests.exceptions.RequestException:
                print("Encountered a requests error! Sleeping for 30s...")
                sleep(30)
            else:
                status, h = cls.inspect_page(x.content)
                if status:
                    last_page = h[0]
                    parsed_page = h[1]
                    break
                else:
                    if "error has occurred" in h:
                        print("An 'unexpected' error has occured. "
                              "Sleeping for 30s...")
                        sleep(30)

        for li in cls.like_page_xpath(parsed_page):
            likedict = {
                'Username' : li[0].text,
                'UserID' : cls.posterID_re.search(li[0].attrib['href']).group(1)
                            }
            likes.append(likedict)

        return likes

    @classmethod
    def unescape(cls, text):
        def fixup(m):
            text = m.group(0)
            if text[:2] == "&#":
                # character reference
                try:
                    if text[:3] == "&#x":
                        return chr(int(text[3:-1], 16))
                    else:
                        return chr(int(text[2:-1]))
                except ValueError:
                    pass
            else:
                # named entity
                try:
                    text = chr(entities.name2codepoint[text[1:-1]])
                except KeyError:
                    pass
            return text # leave as is
        return re.sub("&#?\w+;", fixup, text).strip()

    @classmethod
    def parse_page(cls, page):
        unep = cls.unescape(page.decode()).encode()
        return etree.parse(BytesIO(unep), cls.magical_parser).getroot()

    @classmethod
    def break_page(cls, parsedpage, usernames):
        cleaned_list = []
        post_li = cls.post_xpath(parsedpage)
        p_info = cls.page_info(parsedpage)
        for post in post_li:
            try:
               po_info = cls.post_info(post, usernames)
            except ValueError:
                continue
            po_info.update(p_info)
            po_info["PostContents"] = cls.clean_text(post)
            # with open('blei.txt', 'a') as f:
            #     f.write(po_info["PostContents"])
            #     f.write('\n\n+++++++++++++++++\n\n')
            cleaned_list.append(po_info)
        return cleaned_list

    @classmethod
    def inspect_page(cls, page):
        try:
            cls.root = cls.parse_page(page)
        except TypeError:
            return False, page

        try:
            return False, cls.error_xpath(cls.root)[0].text
        except IndexError:
            try:
                return True, (cls.end_xpath(cls.root)[0].get("data-last"), 
                              cls.root)
            except IndexError:
                return True, (0, cls.root)


if __name__ == "__main__":
    import json

    with open("example.txt", "r") as f:
        example = f.read().encode()

    XFparser = XenforoToBBCode()
    par = XFparser.parse_page(example)

    pam = XFparser.break_page(par, set(["Firnagzen"]))

    with open("dump.txt", "w") as f:
        f.write('[\n')
        for i in pam:
            f.write(json.dumps(i, indent=4, separators=(',', ': ')))
            f.write("\n,\n")

        f.write(json.dumps(i, indent=4, separators=(',', ': ')))
        f.write('\n]')
